" See: https://github.com/vim/vim/issues/3117
if has('python3') && !has('patch-8.1.201')
	silent! python3 1
endif

let mapleader = ","

set t_Co=256
set tabstop=4
set backspace=start,indent,eol
set noswapfile
set undolevels=100
set autoread

" Omni completion provides smart autocompletion for programs.
" http://vim.wikia.com/wiki/Omni_completion
filetype plugin on
set omnifunc=syntaxcomplete#Complete
set completeopt=longest,menuone
"inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"inoremap <expr> <C-n> pumvisible() ? '<C-n>' : '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
"inoremap <expr> <M-,> pumvisible() ? '<C-n>' : '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

call plug#begin()

Plug 'editorconfig/editorconfig-vim'

" File browser
Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeToggle', 'NERDTreeFind'] }
let g:NERDTreeWinSize = 40
let g:NERDTreeShowBookmarks = 1
let g:NERDTreeBookmarksSort = 1
let g:NERDTreeChDirMode = 2
let g:NERDTreeShowHidden = 1
nnoremap <C-o> :NERDTreeToggle<CR>
nnoremap <C-l> :NERDTreeFind<CR>

" Comment lines with command + /
Plug 'tomtom/tcomment_vim', { 'on': 'TComment' }
nnoremap <D-/> :TComment<CR>
vnoremap <D-/> :TComment<CR>

" Fuzzy finder
let $FZF_DEFAULT_OPTS = '--reverse'
let g:fzf_layout = { 'up': '~40%' }
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'
nnoremap <C-f> :Files<CR>
nnoremap  :Buffers<CR>
nnoremap  :BTags<CR>
nnoremap <C-r> :History<CR>

" Puppet support
autocmd BufRead,BufNewFile *.pp set filetype=puppet
Plug 'rodjek/vim-puppet', { 'for': 'puppet' }

" Git integration
Plug 'tpope/vim-fugitive'
nnoremap <silent> <leader>gb :Gblame<CR>
nnoremap <silent> <leader>co :call fzf#run(fzf#wrap({
	\ 'source': 'git branch --format="%(refname:short)"',
	\ 'sink': 'Git checkout'
	\ }))<CR>

" Change current working directory to project root
Plug 'airblade/vim-rooter'
let g:rooter_silent_chdir = 1

" Fade inactive windows
Plug 'blueyed/vim-diminactive'

" Multiple cursors (Ctrl-N)
Plug 'terryma/vim-multiple-cursors'

Plug 'SirVer/ultisnips'
let g:UltiSnipsExpandTrigger="<s-tab>"
let g:UltiSnipsJumpForwardTrigger="<s-tab>"

" Bbye allows you to do delete buffers without closing your windows
Plug 'moll/vim-bbye'
nnoremap <Leader>x :Bdelete<CR>

" This is the official PHP indentation plug-in for VIm
Plug '2072/PHP-Indenting-for-VIm'

" An up-to-date Vim syntax for PHP
Plug 'StanAngeloff/php.vim'
let g:php_var_selector_is_identifier=1

call plug#end()

" Configure the GUI
if has('gui_running')
	set guifont=Monaco:h13
	set linespace=5
	set guicursor+=a:blinkon0
	colorscheme macvim
endif

nnoremap <Tab> w
nnoremap K ciw

" Line numbers
nnoremap <Leader>n :set nu!<CR>
hi LineNr ctermbg=White ctermfg=LightGray guibg=White guifg=ivory3

" Show tabs and trailing spaces
set listchars=tab:>-
set listchars+=trail:◦
set list
nnoremap <Leader>l :set list!<CR>
hi SpecialKey ctermfg=LightGray guifg=LightGray

" Show vertical line at column 80
set colorcolumn=80
hi ColorColumn guibg=gray98 ctermbg=255

" Jump to the next / previous buffer with arrow keys
nnoremap <silent> <Right> :bn<CR>
nnoremap <silent> <Left> :bN<CR>

hi Comment ctermfg=LightGray guifg=LightGray

" Search settings
set incsearch
set ignorecase
set smartcase
nnoremap <Space> /

" Status line
set laststatus=2
set statusline=%=%R%H\ %((%{fugitive#head(8)})%)
hi StatusLine guibg=gray98 guifg=Brown ctermfg=255 ctermbg=Black

" Open SourceTree
nnoremap <silent> <leader>st :silent execute "!open -a SourceTree $PWD"<CR>
