Prerequisites
-------------
- Vim 8.x
- Homebrew

How to install?
---------------
Clone the git repository:

```
git clone ... ~/.vim
```

Run the setup script:

```
cd ~/.vim && make
```
