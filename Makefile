gui_padding := 10

.PHONY: setup
setup: | ~/.vim/plugged /usr/local/opt/fzf
	defaults write org.vim.MacVim MMTextInsetTop $(gui_padding)
	defaults write org.vim.MacVim MMTextInsetLeft $(gui_padding)
	defaults write org.vim.MacVim MMTextInsetBottom $(gui_padding)
	defaults write org.vim.MacVim MMTextInsetRight $(gui_padding)

~/.vim/plugged: | ~/.vim/autoload/plug.vim
	vim +PlugInstall +qall

~/.vim/autoload/plug.vim:
	curl -fLo ~/.vim/autoload/plug.vim \
	--create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

/usr/local/opt/fzf:
	brew install fzf

.PHONY: update
update:
	vim +PlugUpdate +qall
